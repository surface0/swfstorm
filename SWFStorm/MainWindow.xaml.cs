﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Interop;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Linq;

using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

using Microsoft.WindowsAPICodePack.Dialogs;
using SWFStorm.Swf;

namespace SWFStorm
{
    using Path = System.IO.Path;

    internal class ImageListViewItem
    {
        public DefineBits DefineBits { get; set; }
        public string Replace { get; set; }
    }

    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<ImageListViewItem> listViewItems;
        private string xml;

        public MainWindow()
        {
            InitializeComponent();
            this.MouseLeftButtonDown += (sender, e) => this.DragMove();
            this.Title = "SWFStorm（仮）";
        }

        private void WindowIcon_MouseDown(object sender, MouseButtonEventArgs e)
        {
            SystemCommands.ShowSystemMenu(this, this.PointToScreen(new System.Windows.Point(0, 0)));
            e.Handled = true;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
        }

        private void listview_SourceUpdated(object sender, DataTransferEventArgs e)
        {
            var grid = (GridView)((ListView)sender).View;
            foreach (var c in grid.Columns)
            {
                c.Width = 1;
                c.Width = double.NaN;
            }
        }

        private void loadSwf(string filename)
        {
            this.xml = Swfmill.SwfToXml(filename);
            var dr = new DefineBitsReader(this.xml);
            listViewItems = (from x in dr.ReadAll() select new ImageListViewItem { DefineBits = x, Replace = "" }).ToList();
            this.DataContext = listViewItems;
        }

        private void Window_Drop(object sender, DragEventArgs e)
        {
            loadSwf(((string[])e.Data.GetData(DataFormats.FileDrop))[0]);
        }

        private void Window_PreviewDragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var file = ((string[])e.Data.GetData(DataFormats.FileDrop))[0];
                e.Effects = Path.GetExtension(file) == ".swf" ? DragDropEffects.Copy : DragDropEffects.None;
            }
            e.Handled = true;
        }

        private void replaceByString(object sender, RoutedEventArgs e)
        {
            var item = (ImageListViewItem)listview.SelectedItem;
            var ird = new InputReplaceStringDialogBox();
            ird.ReplaceString.Text = item.Replace;
            if (ird.ShowDialog() == true && ird.ReplaceString.Text.Trim().Length > 0)
            {
                var i = this.listViewItems.IndexOf(item);
                this.listViewItems[i].Replace = ird.ReplaceString.Text;
                listview.Items.Refresh();
            }
        }

        private void export(object sender, RoutedEventArgs e)
        {
            //TODO:ファイルに書き出し（JPG/GIF/PNG）
            using (var sfd = new CommonSaveFileDialog())
            {
                sfd.Filters.Add(new CommonFileDialogFilter("PNG", "*.png"));
                sfd.Filters.Add(new CommonFileDialogFilter("JPEG", "*.jpg;*.jpeg"));
                sfd.Filters.Add(new CommonFileDialogFilter("BMP", "*.bmp"));
                sfd.DefaultExtension = "png";
                sfd.AlwaysAppendDefaultExtension = true;

                if (sfd.ShowDialog() == CommonFileDialogResult.Ok)
                {
                    var bmp = ((ImageListViewItem)listview.SelectedItem).DefineBits.Bitmap;
                    bmp.Save(sfd.FileName);
                    MessageBox.Show("保存が完了しました。");
                }
            }
        }

        private void copyToClipboard(object sender, RoutedEventArgs e)
        {
            //TODO:BmpとBitmapImageの相互変換はSDFLibに組み込みたい。
            var bmp = ((ImageListViewItem)listview.SelectedItem).DefineBits.Bitmap;
            var img = Imaging.CreateBitmapSourceFromHBitmap(
                bmp.GetHbitmap(),
                IntPtr.Zero,
                Int32Rect.Empty,
                BitmapSizeOptions.FromEmptyOptions());
            Clipboard.SetImage(img);
        }

        private void SaveAs_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            using (var sfd = new CommonSaveFileDialog())
            {
                sfd.Filters.Add(new CommonFileDialogFilter("XMLドキュメント", "*.xml"));
                sfd.DefaultExtension = "xml";
                sfd.AlwaysAppendDefaultExtension = true;

                if (sfd.ShowDialog() == CommonFileDialogResult.Ok)
                {
                    var dw = new DefineBitsWriter(this.xml);
                    foreach (var item in this.listViewItems)
                    {
                        var i = (ImageListViewItem)item;
                        if (i.Replace != "")
                        {
                            dw.WriteString(i.DefineBits.ObjectID, i.Replace);
                        }
                    }
                    dw.Xml.Save(sfd.FileName);
                    MessageBox.Show("保存が完了しました。");
                }
            }
        }

        private void Close_Executed(object sender, ExecutedRoutedEventArgs e)
        {
        }
    }
}

