﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml;

namespace SWFStorm.Swf
{
    public class DefineBitsReader
    {
        protected string[] supportTags = new string[] {
            "DefineBits",
            "DefineBitsJPEG2",
            "DefineBitsJPEG3",
            "DefineBitsLossless",
            "DefineBitsLossless2"
        };

        public XElement Xml { get; protected set; }
        protected JPEGTables jpegTables;
        protected IEnumerator<XElement> targetElements;

        public DefineBitsReader(string xml)
        {
            Xml = XElement.Parse(xml);
            initJpegTables();
            initTargetElements();
        }

        private void initTargetElements()
        {
            var elements = new List<XElement>();

            targetElements = (from x in Xml.Descendants()
                             where supportTags.Contains(x.Name.ToString())
                             select x).GetEnumerator();
        }

        public void initJpegTables()
        {
            jpegTables = null;
            //UnknownTag id="0x08"がJPEGTableらしい
            var elm = from x in Xml.Descendants("UnknownTag") where x.Attribute("id").Value == "0x08" select x;
            if (elm.Count() > 1)
            {
                throw new SwfmillException(Resources.MessagesResource.DefineBitsReaderManyJPEGTables);
            }
            else if (elm.Count() == 1)
            {
                jpegTables = new JPEGTables(elm.First());
            }
        }

        protected DefineBits createDefineBits(XElement elm)
        {
            switch (elm.Name.ToString())
            {
                case "DefineBits":
                    return new DefineBits(elm, jpegTables);
                case "DefineBitsJPEG2":
                    return new DefineBitsJPEG2(elm);
                case "DefineBitsJPEG3":
                    return new DefineBitsJPEG3(elm);
                case "DefineBitsLossless":
                    return new DefineBitsLossless(elm);
                case "DefineBitsLossless2":
                    return new DefineBitsLossless2(elm);
                default:
                    return null;
            }
        }

        public DefineBits Read()
        {
            if (targetElements.MoveNext())
            {
                var elm = targetElements.Current;
                return createDefineBits(elm);
            }

            return null;
        }

        public IEnumerable<DefineBits> ReadAll()
        {
            List<DefineBits> defineBitsList = new List<DefineBits>();
            DefineBits defineBits;

            while ((defineBits = Read()) != null)
            {
                defineBitsList.Add(defineBits);
            }

            return defineBitsList;
        }
    }
}
