﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Drawing.Drawing2D;
using System.Drawing;
using System.Drawing.Imaging;
using System.Xml.Linq;
using System.Xml;

using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace SWFStorm.Swf
{
    public static class Swfmill
    {
        public const string SWFMILL_EXE = @"swfmill.exe";

        public static string SwfToXml(string filename)
        {          
            var psi = new ProcessStartInfo()
            {
                FileName = SWFMILL_EXE,
                CreateNoWindow = true,
                UseShellExecute = false,
                RedirectStandardInput = false,
                RedirectStandardOutput = true,
                Arguments = String.Format("swf2xml \"{0}\"", filename)
            };
            
            var p = Process.Start(psi);
            var xml = p.StandardOutput.ReadToEnd();
            p.WaitForExit();

            return xml;
        }
    }
}
