﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWFStorm.Swf
{
    public class SwfmillException : Exception
    {
        public SwfmillException()
        {
        }

        public SwfmillException(string message)
            : base(message)
        {
        }

        public SwfmillException(string message, Exception inner)
            : base(message)
        {
        }
    }
}
