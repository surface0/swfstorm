﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Xml.Linq;
using System.IO;
using System.IO.Compression;

namespace SWFStorm.Swf
{
    /// <summary>
    /// 透過色が無いロスレスフォーマット（PNG/GIF）
    /// </summary>
    public class DefineBitsLossless : DefineBits
    {
        protected int format;
        protected int width;
        protected int height;
        protected int nColorMap;

        public DefineBitsLossless(XElement elm) : base(elm)
        {
        }

        protected override void initialize()
        {
            var elm = this.xelement;
            format = Int32.Parse(elm.Attribute("format").Value);
            nColorMap = Int32.Parse(elm.Attribute("n_colormap").Value);
            width = Int32.Parse(elm.Attribute("width").Value);
            height = Int32.Parse(elm.Attribute("height").Value);
            ObjectID = this.xelement.Attribute("objectID").Value;
            Bitmap = decodeImage();
            ExtraInfo = String.Format("format={0}\nn_colormap={1}", format, nColorMap);
        }

        protected override Bitmap decodeImage()
        {
            switch (format)
            {
                case 3:
                    return decodeDataFormat3();
                case 4:
                    return decodeDataFormat4();
                case 5:
                    return decodeDataFormat5();
                default:
                    return null;
            }
        }

        /// <summary>
        /// パレットカラー形式（GIF）
        /// </summary>
        /// <returns></returns>
        protected virtual Bitmap decodeDataFormat3()
        {
            var data = this.xelement.Element("data").Element("data").Value;
            var src = Convert.FromBase64String(data);
            var bmp = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);

            using (var ms = new MemoryStream())
            {
                ms.Write(this.gzMagicHeader, 0, this.gzMagicHeader.Length);
                ms.Write(src, 0, src.Length);
                ms.Seek(0, SeekOrigin.Begin);
                var gs = new GZipStream(ms, CompressionMode.Decompress);
                var colormap = readColorMap(gs);

                var br = new BinaryReader(gs);
                for (var iy = 0; iy < height; iy++)
                {
                    var indices = br.ReadBytes(((width + 3) & -4)); // 行ごとに4バイト境界が適用されている
                    for (var ix = 0; ix < width; ix++)
                    {
                        bmp.SetPixel(ix, iy, colormap[indices[ix]]);
                    }
                }

                gs.Close();
            }

            return bmp;
        }

        /// <summary>
        /// カラーマップを取得する。
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        protected virtual Color[] readColorMap(Stream stream)
        {
            var br = new BinaryReader(stream);
            var colormap = new System.Drawing.Color[nColorMap + 1];
            for (var i = 0; i < nColorMap + 1; i++)
            {
                var rgb = br.ReadBytes(3);
                colormap[i] = System.Drawing.Color.FromArgb(rgb[0], rgb[1], rgb[2]);
            }

            return colormap;
        }

        /// <summary>
        /// colormap を 0RRRRRGGGGGBBBBB の 16bit (BigEndian)で表現する形式。
        /// 未実装。サンプル求む。
        /// </summary>
        /// <returns></returns>
        protected virtual Bitmap decodeDataFormat4()
        {
            return null;
        }

        /// <summary>
        /// ビットマップ形式（PNG)
        /// DefineBitsLosslessではピクセルあたり3バイトしか使用しないが1バイト分paddingされている。
        /// </summary>
        /// <returns></returns>
        protected virtual Bitmap decodeDataFormat5()
        {
            var data = this.xelement.Element("data").Element("data").Value;
            var src = Convert.FromBase64String(data);
            var bmp = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);

            using (var ms = new MemoryStream())
            {
                ms.Write(this.gzMagicHeader, 0, this.gzMagicHeader.Length);
                ms.Write(src, 0, src.Length);
                ms.Seek(0, SeekOrigin.Begin);
                var gs = new GZipStream(ms, CompressionMode.Decompress);
                var br = new BinaryReader(gs);

                for (var iy = 0; iy < height; iy++)
                {
                    for (var ix = 0; ix < width; ix++)
                    {
                        var value = br.ReadInt32();
                        bmp.SetPixel(ix, iy, Color.FromArgb(value));
                    }
                }

                gs.Close();
            }

            return bmp;
        }
    }
}
