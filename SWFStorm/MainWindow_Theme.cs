﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SWFStorm
{
    /// <summary>
    /// ウィンドウ状態に対応する色
    /// </summary>
    internal sealed class WindowStateColors
    {
        public static Color Default { get { return Color.FromRgb(104, 33, 122); } }
        public static Color Active { get { return Color.FromRgb(0, 122, 204); } }
        public static Color Busy { get { return Color.FromRgb(202, 81, 00); } }
        public static Color Deactive { get { return Color.FromRgb(99, 99, 99); } }
    }

    public partial class MainWindow : Window
    {
        /// <summary>
        /// ウィンドウ状態。
        /// </summary>
        private Color windowStateColor = WindowStateColors.Default;

        #region イベントハンドラ
        private void Window_ActivateChanged(object sender, EventArgs e)
        {
            updateWindowVisualState();
        }


        private void MinimizeWindowButton_Click(object sender, RoutedEventArgs e)
        {
            SystemCommands.MinimizeWindow(this);
        }

        private void MaxmaizeWindowButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.WindowState == WindowState.Normal)
            {    
                SystemCommands.MaximizeWindow(this);   
            }
            else
            {
                SystemCommands.RestoreWindow(this);   
            }
            onWindowStateChange();
        }

        private void Window_StateChanged(object sender, EventArgs e)
        {
            onWindowStateChange();
        }

        private void CloseWindowButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        #endregion

        /// <summary>
        /// ウィンドウの状態が変化した場合に呼び出すメソッド。
        /// </summary>
        private void onWindowStateChange()
        {
            if (this.WindowState == WindowState.Maximized)
            {
                this.maximizeWindowButton.Content = "2";
                //this.windowBorder.Margin = new Thickness(7f);
            }
            else if (this.WindowState == WindowState.Normal)
            {
                this.maximizeWindowButton.Content = "1";
                //this.windowBorder.Margin = new Thickness(0f);
            }
        }

        /// <summary>
        /// ウィンドウを現在の状態に合わせて更新する。
        /// </summary>
        private void updateWindowVisualState()
        {
            updateWindowVisualState(this.windowStateColor);
        }

        /// <summary>
        /// ウィンドウを現在の状態に合わせて更新し、ステータスバーにテキストを表示する。
        /// </summary>
        /// <param name="statusLabel">ステータスバーに表示するテキスト</param>
        private void updateWindowVisualState(string statusLabel)
        {
            updateWindowVisualState(this.windowStateColor, statusLabel);
        }

        /// <summary>
        /// ウィンドウの状態と表示を更新し、ステータスバーにテキストを表示する。
        /// </summary>
        /// <param name="color"></param>
        /// <param name="statusLabel"></param>
        private void updateWindowVisualState(Color color, string statusLabel = null)
        {
            this.windowStateColor = color;
            //this.windowBorder.BorderBrush = new SolidColorBrush(this.IsActive ? color : WindowStateColors.Deactive);
            this.statusBar.Background = new SolidColorBrush(color);
            
            if (statusLabel != null)
            {
                this.statusStripLabel.Content = statusLabel;
            }
        }
    }
}
